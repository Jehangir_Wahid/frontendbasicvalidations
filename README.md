The code is intended to apply the following basic validations on the HTML-Form.

The form is validated against:
1.	Required Field Validation.
2.	Data Format Validation (Through regex).
3.	Data maximum-length and maximum-length Validation(Through regex).

Please go through the code and suggest, if any optimization or refinery is needed.
Thank you